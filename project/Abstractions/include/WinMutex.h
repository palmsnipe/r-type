#include <windows.h>
#include "IMutex.h"

#ifndef __WINMUTEX_H__
#define __WINMUTEX_H__

class WinMutex : public IMutex
{
public:
	WinMutex();
	virtual ~WinMutex();

	virtual void			lock();
	virtual void			unlock();
	virtual bool			trylock();
private:
	HANDLE					_mutex;
};

#endif