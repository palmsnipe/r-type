#include <pthread.h>
#include "IThread.h"

#ifndef __LINUXTHREAD_H__
#define __LINUXTHREAD_H__

class LinuxThread : public IThread
{
public:
	LinuxThread();
	virtual ~LinuxThread();

	virtual bool			create(void* (*func)(void*), void* arg);
	virtual bool			run();
	virtual void			close();
	virtual ThreadState		getState() const;
private:
	ThreadState				_state;
	pthread_t				_thread;
};

#endif