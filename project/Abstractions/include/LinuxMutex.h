#include <pthread.h>
#include "IMutex.h"

#ifndef __LINUXMUTEX_H__
#define __LINUXMUTEX_H__

class LinuxMutex : public IMutex
{
public:
	LinuxMutex();
	virtual ~LinuxMutex();

	virtual void			lock();
	virtual void			unlock();
	virtual bool			trylock();
private:
	pthread_mutex_t			_mutex;
};

#endif