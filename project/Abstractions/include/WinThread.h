#include <windows.h>
#include "IThread.h"

#ifndef __WINTHREAD_H__
#define __WINTHREAD_H__

class WinThread : public IThread
{
public:
	WinThread();
	virtual ~WinThread();

	virtual bool			create(void* (*func)(void*), void* arg);
	virtual bool			run();
	virtual void			close();
	virtual ThreadState		getState() const;
private:
	ThreadState				_state;
	HANDLE					_thread;
};

#endif