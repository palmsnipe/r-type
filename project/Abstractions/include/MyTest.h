
#ifndef __MYTEST_H__
#define __MYTEST_H__
#ifdef _WIN32
#  define EXPORTIT __declspec( dllexport )
#else
#  define EXPORTIT
#endif

EXPORTIT int somefunction();

class MyTest
{
public:
	MyTest();
	~MyTest();

	void		print();
};

#endif