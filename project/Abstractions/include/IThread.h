#ifndef __ITHREAD_H__
#define __ITHREAD_H__

enum ThreadState
{
	ALIVE,
	RUNNING,
	DEAD
};

class IThread
{
public:
	virtual ~IThread {}

	virtual bool			create(void* (*func)(void*), void* arg) = 0;
	virtual bool			run() = 0;
	virtual void			close() = 0;
	virtual ThreadState		getState() const = 0;
};

#endif