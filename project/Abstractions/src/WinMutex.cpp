#include <windows.h>
#include <iostream>
#include "WinMutex.h"

WinMutex::WinMutex()
{
	_mutex = CreateMutex(NULL, FALSE, NULL);
}

WinMutex::~WinMutex()
{
	CloseHandle(_mutex);
}

void			WinMutex::lock()
{
	WaitForSingleObject(_mutex, INFINITE);
}

void			WinMutex::unlock()
{
	ReleaseMutex(mutex_);
}


bool			WinMutex::trylock()
{
	WaitForSingleObject(_mutex, 0);
}