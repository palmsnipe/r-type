#include <pthread.h>
#include "LinuxThread.h"

LinuxThread::LinuxThread()
{

}

LinuxThread::~LinuxThread()
{

}

bool			LinuxThread::create(void* (*func)(void*), void* arg)
{
	if (pthread_create(&(_thread), NULL, func, arg) != 0)
		return false;
	_state = ALIVE;

	return true;
}

bool			LinuxThread::run()
{
	if (pthread_join(_thread, NULL) != 0)
		return false;
	_state = RUNNING;

	return true;
}

void			LinuxThread::close()
{
	pthread_exit(NULL);
}

ThreadState		LinuxThread::getState() const
{
	return _state;
}