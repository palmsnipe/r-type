#include <iostream>
#include "LinuxMutex.h"

LinuxMutex::LinuxMutex()
{
	pthread_mutex_init(&_mutex, NULL);
}

LinuxMutex::~LinuxMutex()
{

}

void			LinuxMutex::lock()
{
	pthread_mutex_lock(&_mutex);
}

void			LinuxMutex::unlock()
{
	pthread_mutex_unlock(&_mutex);
}


bool			LinuxMutex::trylock()
{
	return pthread_mutex_trylock(&_mutex);
}