#include <windows.h>
#include "WinThread.h"

WinThread::WinThread()
{

}

WinThread::~WinThread()
{

}

bool			WinThread::create(void* (*func)(void*), void* arg)
{
	_thread = CreateThread(NULL, 0, func, arg, 0, NULL);
	if (_thread == INVALID_HANDLE_VALUE)
		return false;
	_state = ALIVE;

	return true;
}

bool			WinThread::run()
{
	WaitForSingleObject(_thread, INFINITE);
	_state = RUNNING;

	return true;
}

void			WinThread::close()
{
	pthread_exit(NULL);
	TerminateThread(_thread, 0);
}

ThreadState		WinThread::getState() const
{
	return _state;
}