#ifndef __MYIMAGE_HH__
#define __MYIMAGE_HH__

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>


class MyImage
{
public:
	MyImage();
	MyImage(std::string, float, float, int, int);
	MyImage(MyImage&);
	~MyImage(void);

	void			operator=(MyImage&);

	void			setSprite(sf::Sprite);
	sf::Sprite&		getSprite();

	void			setTexture(sf::Texture);
	sf::Texture&		getTexture();

	void			setTexturePath(std::string);
	std::string		getTexturePath();

	void			setPosX(float);
	float			getPosX();

	void			setPosY(float);
	float				getPosY();

	void			setWidth(int);
	int				getWidth();

	void			setLength(int);
	int				getLength();

private:
	sf::Sprite		_sprite;
	sf::Texture		_texture;
	std::string		_texturePath;
	float			_x,_y;
	int				_width,_length;
};

#endif