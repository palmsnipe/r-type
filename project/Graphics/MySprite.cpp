#include "MySprite.hh"


MySprite::MySprite(void)
	: Sprite()
{
}


MySprite::~MySprite(void)
{
}

void		MySprite::setTexture(MyTexture newTexture)
{
	sf::Sprite::setTexture(newTexture);
}

void		MySprite::setPos(float x, float y)
{
	sf::Sprite::setPosition(x, y);
}

void		MySprite::setPos(t_fpos pos)
{
	sf::Sprite::setPosition(pos.x,pos.y);
}

t_fpos		MySprite::getPos()
{
	t_fpos		ret;

	ret.x = (sf::Sprite::getPosition()).x;
	ret.y = (sf::Sprite::getPosition()).y;
	return ret;
}

void		MySprite::setAngleRotate(float angle)
{
	sf::Sprite::setRotation(angle);
}

float		MySprite::getAngleRotate()
{
	return sf::Sprite::getRotation();
}

void		MySprite::addRotate(float angleToAdd)
{
	sf::Sprite::rotate(angleToAdd);
}

void		MySprite::setTranformPoint(t_fpos pos)
{
	sf::Sprite::setOrigin(pos.x, pos.y);
}

t_fpos		MySprite::getTranformPoint()
{
	t_fpos		ret;

	ret.x = sf::Sprite::getOrigin().x;
	ret.y = sf::Sprite::getOrigin().y;
	return ret;
}

void		MySprite::move(t_fpos pos)
{
	sf::Sprite::move(pos.x, pos.y);
}