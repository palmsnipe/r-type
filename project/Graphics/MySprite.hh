#ifndef __MYSPRITE_HH__
#define __MYSPRITE_HH__

#include <SFML/Graphics/Sprite.hpp>
#include "MyTexture.hh"

typedef struct	s_fpos
{
	float	x;
	float	y;
}				t_fpos;

class MySprite :	public sf::Sprite
{
public:
	MySprite(void);
	~MySprite(void);

	void		setTexture(MyTexture);

	void		setPos(float, float);
	void		setPos(t_fpos);
	t_fpos		getPos();

	void		setAngleRotate(float);
	float		getAngleRotate();
	void		addRotate(float);

	void		setTranformPoint(t_fpos);
	t_fpos		getTranformPoint();

	void		move(t_fpos);

private :

};

#endif