#include "MyTexture.hh"


MyTexture::MyTexture(void)
	: sf::Texture()
{
}

MyTexture::~MyTexture(void)
{
}

bool		MyTexture::create(int width, int heigth)
{
	return sf::Texture::create(width, heigth);
}

bool		MyTexture::loadFromFile(std::string path)
{
	return sf::Texture::loadFromFile(path);
}

void		MyTexture::operator=(sf::Texture other)
{
	
}