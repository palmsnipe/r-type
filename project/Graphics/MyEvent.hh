#ifndef __MYEVENT_HH__
#define __MYEVENT_HH__

#include <SFML/Window/Event.hpp>


class MyEvent : public sf::Event
{
public:
	enum Key
{
	A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Z,
	NUM0,NUM1,NUM2,NUM3,NUM4,NUM5,NUM6,NUM7,NUM8,NUM9,
	ESC,LCONTROL,LSHIFT,LALT,LSYSTEM,
	RCONTROL,RSHIFT,RALT,RSYSTEM,
	KUP,KDOWN,KLEFT,KRIGHT,
	NBKEY
};

	enum MyMouseButton
{
	BLEFT,
	BRIGHT,
	BMIDDLE,
	NBBUTTON
};

	enum MyEventType
	{
		CLOSED,
		KEY_PRESSED,
		KEY_RELEASED,
		MOUSE_BUTTON_PRESSED,
        MOUSE_BUTTON_RELEASED,
        MOUSE_MOVED,
		NBEVENT
	};

	struct MyMouseMoveEvent
	{
		int		x;
		int		y;
	};

	struct MyMouseButtonEvent
	{
		MyMouseButton	button;
		int				x;
		int				y;
	};

	struct MyKeyEvent
	{
		Key		key;
		bool	alt;
		bool	control;
		bool	shift;
	};

	MyEventType		_type;

	union
	{
		MyKeyEvent				key;
		MyMouseMoveEvent		mouseMove;
		MyMouseButtonEvent		mouseButton;
	}val;
};

#endif