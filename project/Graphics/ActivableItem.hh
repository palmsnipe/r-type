#ifndef __ACTIVABLEITEM_HH__
#define __ACTIVABLEITEM_HH__

#include "abstractitem.hh"
#include <list>


typedef	void	(*PtrFunc)(void);


class ActivableItem :	public AbstractItem
{
public:
	ActivableItem(void);
	virtual ~ActivableItem(void);

	bool				isOnArea(t_pos);
	void				clicked();

	void				setLabel(std::string);
	std::string			getLabel();

	void				setPathActiveImage(std::string);
	std::string			getPathActiveImage();

	void				setPFunc(PtrFunc);

protected:
	std::string			_label;
	PtrFunc				_pfunc;
};

#endif