#include "AbstractItem.hh"


AbstractItem::AbstractItem(void)
{
}


AbstractItem::~AbstractItem(void)
{
}

void						AbstractItem::setName(std::string newName)
{
	_name = newName;
}

std::string					AbstractItem::getName()
{
	return _name;
}

void						AbstractItem::setSprite(MySprite newSprite)
{
	_sprite = newSprite;
}

MySprite					AbstractItem::getSprite()
{
	return _sprite;
}

void						AbstractItem::setTexture(MyTexture newTexture)
{
	_Texture = newTexture;
}

MyTexture						AbstractItem::getTexture()
{
	return _Texture;
}

void						AbstractItem::setFirstPath(std::string newPath)
{
	_listPaths[0] = newPath;
}

std::string					AbstractItem::getFirstPath()
{
	return _listPaths[0];
}

std::vector<std::string>		AbstractItem::getListPaths()
{
	return _listPaths;
}

void						AbstractItem::setInitPos(t_pos newPos)
{
	_initPos = newPos;
}
t_pos						AbstractItem::getInitPos()
{
	return _initPos;
}

void						AbstractItem::setArea(t_area newArea)
{
	_area = newArea;
}

t_area						AbstractItem::getArea()
{
	return _area;
}