//#include "Item.hh"

#include "MyWindow.hh"
#include "MyEvent.hh"
#include "Item.hh"
#include <iostream>

#include "MyImage.h"

int main()
{
	MyWindow	window(800,600, "My window");
	MyImage		image1("sfmlTest1.png", 0.f, 0.f, 800, 600);
	MyImage		image2("sfmlTest2.png", 0.f, 0.f, 600, 800);
	MyImage		currentImage(image1);

	//system("PAUSE");
	while (window.Open())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		MyEvent event;
		while (window.pollMyEvent(event))
		{
			//t_pos	mousePos;
			//mousePos.x = event.val.mouseMove.x;
			//mousePos.y = event.val.mouseMove.y;
			//std::cout << "mouseX : " << mousePos.x << std::endl;
			//std::cout << "mouseY : " << mousePos.y << std::endl;
			if (event._type == MyEvent::MOUSE_MOVED && event.val.mouseMove.y > 300)
			{
				std::cout << "IMAGE1" << std::endl;
				currentImage.setTexture(image1.getTexture());
			}
			if(event._type == MyEvent::MOUSE_MOVED && event.val.mouseMove.y <= 300)
			{
				std::cout << "IMAGE2" << std::endl;
				currentImage.setTexture(image2.getTexture());
			}
			else if (event._type == MyEvent::KEY_PRESSED && event.val.key.key == MyEvent::KUP)
			{
				std::cout << "Key UP pressed" << std::endl;
				currentImage.setTexture(image1.getTexture());
			}
			else if (event._type == MyEvent::KEY_PRESSED && event.val.key.key == MyEvent::KDOWN)
			{
				std::cout << "Key DOWN pressed" << std::endl;
				currentImage.setTexture(image2.getTexture());
			}
			//if (event.type == sf::Event::MouseMoved && item1.mouseOnArea(mousePos) ==  true)	
				//window.draw(image1);
			
			// "close requested" event: we close the window
			if (event._type == MyEvent::CLOSED)
			{
				std::cout << "CLOSE NOW !" <<std::endl;
				window.close();
			}
		}
		window.draw(currentImage);
		window.myDisplay();

		//system("PAUSE");
	}
	//temoin.display();
		
	return 0;
}