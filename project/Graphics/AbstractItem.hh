#ifndef __ABSTRACTITEM_HH__
#define __ABSTRACTITEM_HH__

#include <string>
#include <vector>
#include "MySprite.hh"
#include "MyTexture.hh"

typedef struct	s_pos
{
int	x;
int	y;
}				t_pos;

typedef struct	s_area
{
t_pos	TL;
t_pos	BR;
}				t_area;

class AbstractItem
{
public:
	AbstractItem(void);
	virtual ~AbstractItem(void);

	void						setName(std::string);
	std::string					getName();
	
	void						setSprite(MySprite);
	MySprite					getSprite();

	void						setTexture(MyTexture);
	MyTexture						getTexture();

	void						setFirstPath(std::string);
	std::string					getFirstPath();

	std::vector<std::string>	getListPaths();

	void						setInitPos(t_pos);
	t_pos						getInitPos();

	void						setArea(t_area);
	t_area						getArea();

protected:
	std::string					_name;
	MySprite					_sprite;
	MyTexture					_Texture;
	std::vector<std::string>	_listPaths;
	t_pos						_initPos;
	t_area						_area;
};

#endif