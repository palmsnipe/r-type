#include "ActivableItem.hh"


ActivableItem::ActivableItem(void)
{
}


ActivableItem::~ActivableItem(void)
{
}

bool				ActivableItem::isOnArea(t_pos mousePos)
{
	if ((mousePos.x >= _area.TL.x && mousePos.x <= _area.BR.x) &&
		(mousePos.y >= _area.TL.y && mousePos.y <= _area.BR.y))
		return true;
	return false;
}


void				ActivableItem::setLabel(std::string newLabel)
{
	_label = newLabel;
}

std::string			ActivableItem::getLabel()
{
	return _label;
}

void				ActivableItem::setPathActiveImage(std::string pathActiveImage)
{
	_listPaths[1] = pathActiveImage;
}

std::string			ActivableItem::getPathActiveImage()
{
	return _listPaths[1];
}

void				ActivableItem::setPFunc(PtrFunc newFunc)
{
	_pfunc = newFunc;
}