#ifndef __MYWINDOW_HH__
#define __MYWINDOW_HH__

#include <SFML/Graphics/RenderWindow.hpp>
#include "MySprite.hh"
#include "MyEvent.hh"
#include "MyImage.h"

class MyWindow :	public sf::RenderWindow
{
public:

	enum WindowSate
	{
		MENU1,
		MENU2,
		GAME
	};

	MyWindow(int, int, std::string);
	~MyWindow(void);

	void				draw(MyImage);
	bool				Open();
	void				myDisplay();
	bool				pollMyEvent(MyEvent&);
	void				myClear();
	void				drawMenu1();
	void				drawMenu2();
	void				drawGame();
	void				close();
//	void				drawWarning(std::string);

private:
	WindowSate			_state;
	sf::RenderWindow	_window;
};

#endif