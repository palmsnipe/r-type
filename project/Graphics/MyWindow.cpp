#include "MyWindow.hh"
#include "MyImage.h"

MyWindow::MyWindow(int heigth, int width, std::string title)
{
	 _window.create(sf::VideoMode(heigth, width), title);
}

MyWindow::~MyWindow(void)
{
}

void				MyWindow::draw(MyImage image)
{
	_window.draw(image.getSprite());
}

bool				MyWindow::Open()
{
	return _window.isOpen();
}

void				MyWindow::myDisplay()
{
	_window.display();
}

MyEvent				fillKey(MyEvent event)
{
	if (event.key.code <= 43)
		event.val.key.key = (MyEvent::Key)event.key.code;
	else
	{
		switch (event.key.code)
		{
		case sf::Keyboard::Left:
			{
				event.val.key.key = MyEvent::KLEFT;
				break;
			}
		case sf::Keyboard::Right:
			{
				event.val.key.key = MyEvent::KRIGHT;
				break;
			}
		case sf::Keyboard::Up:
			{
				event.val.key.key = MyEvent::KUP;
				break;
			}
		case sf::Keyboard::Down:
			{
				event.val.key.key = MyEvent::KDOWN;
				break;
			}
		};
	}
	return event;
}

MyEvent				fillButton(MyEvent event)
{
	if ((event.mouseButton.button > 0) && (event.mouseButton.button < 3))
	{
		event.val.mouseButton.button = (MyEvent::MyMouseButton)event.mouseButton.button;
	}
	return event;
}

bool				MyWindow::pollMyEvent(MyEvent& event)
{
	bool			ret;
	ret = _window.pollEvent(event);
	switch (event.type)
	{
	case sf::Event::Closed :
		{
			event._type = MyEvent::CLOSED;
			std::cout << "event closed" << std::endl;
			break;
		}
	case sf::Event::KeyPressed :
		{
			event._type = MyEvent::KEY_PRESSED;
			std::cout << "event key pressed" << std::endl;
			event = fillKey(event);
			break;
		}
	case sf::Event::KeyReleased :
		{
			event._type = MyEvent::KEY_RELEASED;
			std::cout << "event key released" << std::endl;
			event = fillKey(event);
			break;
		}
	case sf::Event::MouseButtonPressed :
		{
			event._type = MyEvent::MOUSE_BUTTON_PRESSED;
			std::cout << "event mouse button pressed" << std::endl;
			event = fillButton(event);
			break;
		}
	case sf::Event::MouseButtonReleased :
		{
			event._type = MyEvent::MOUSE_BUTTON_RELEASED;
			std::cout << "event mouse button released" << std::endl;
			event = fillButton(event);
			break;
		}
	case sf::Event::MouseMoved :
		{
			event._type = MyEvent::MOUSE_MOVED;
			std::cout << "event mouse moved" << std::endl;
			event.val.mouseMove.x = event.mouseMove.x;
			event.val.mouseMove.y = event.mouseMove.y;
			break;
		}
	};
	return ret;
}

void				MyWindow::myClear()
{
	_window.clear();
}

void				MyWindow::drawMenu1()
{

}

void				MyWindow::drawMenu2()
{

}

void				MyWindow::drawGame()
{

}

void				MyWindow::close()
{
	_window.close();
}