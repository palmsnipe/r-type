#ifndef __ITEM_HH__
#define __ITEM_HH__

#include <string>

typedef struct s_pos
{
int	x;
int	y;
}t_pos;

typedef struct s_area
{
t_pos	TL;
t_pos	BR;
}t_area;

class Item
{
private :
	std::string	_name;
	t_pos		_initPos;
	t_area		_changeStateArea;
	
public:
	Item(std::string);
	Item(std::string, t_pos, t_area);
	~Item();

	bool		mouseOnArea(t_pos);

	void		setName(std::string);
	std::string	getName();

	void		setInitPos(t_pos);
	t_pos		getInitPos();

	void		setChangeStateArea(t_area);
	t_area		getChangeStateArea();
};

#endif