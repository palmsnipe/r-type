#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "MyImage.h"

MyImage::MyImage()
{}

MyImage::MyImage(std::string path, float x, float y, int width, int length)
{
	_texturePath = path;
	_x = x;
	_y = y;
	_width = width;
	_length = length;

	//sf::Texture		texture;
	_texture.create(width,length);
	_texture.loadFromFile(path);
	//sf::Sprite		sprite;
	_sprite.setTexture(_texture);
	_sprite.setPosition(x,y);

	//_texture = texture;
	//_sprite = sprite;
	/*std::cout << "create Texture : " << _texture.create(width, length) << std::endl;
	std::cout << "loadFromFile : " << _texture.loadFromFile(path) << std::endl;
	_sprite.setTexture(_texture);
	_sprite.setPos(x, y);*/

}


MyImage::MyImage(MyImage &other)
{
	_texturePath = other.getTexturePath();
	_x = other.getPosX();
	_y = other.getPosY();
	_width = other.getWidth();
	_length = other.getLength();

	_texture.create(other.getWidth(), other.getLength());
	_texture.loadFromFile(other.getTexturePath());
	_sprite.setTexture(other.getTexture());
	_sprite.setPosition(other.getPosX(), other.getPosY());
}

MyImage::~MyImage(void)
{
}

void		MyImage::operator=(MyImage& other)
{
	/*_texturePath = other.getTexturePath();
	_x = other.getPosX();
	_y = other.getPosY();
	_width = other.getWidth();
	_length = other.getLength();

	_texture.create(other.getWidth(), other.getLength());
	_texture.loadFromFile(other.getTexturePath());
	_sprite.setTexture(other.getTexture());
	_sprite.setPosition(other.getPosX(), other.getPosY());*/
	_sprite = other.getSprite();
	_texture = other.getTexture();
}

void		MyImage::setSprite(sf::Sprite newSprite)
{
	_sprite = newSprite;
}

sf::Sprite&	MyImage::getSprite()
{
	return _sprite;
}

void		MyImage::setTexture(sf::Texture newTexture)
{
	_texture = newTexture;
}

sf::Texture&	MyImage::getTexture()
{
	return _texture;
}

void			MyImage::setTexturePath(std::string newPath)
{
	_texturePath = newPath;
}

std::string		MyImage::getTexturePath()
{
	return _texturePath;
}

void			MyImage::setPosX(float x)
{
	_x = x;
}

float			MyImage::getPosX()
{
	return _x;
}

void			MyImage::setPosY(float y)
{
	_y = y;
}

float				MyImage::getPosY()
{
	return _y;
}

void			MyImage::setWidth(int width)
{
	_width = width;
}

int				MyImage::getWidth()
{
	return _width;
}

void			MyImage::setLength(int length)
{
	_length = length;
}

int					MyImage::getLength()
{
	return _length;
}