#include "Item.hh"


Item::Item(std::string name)
{
	_name = name;
}

Item::Item(std::string name, t_pos initPos, t_area changeStateArea)
{
	_name = name;
	_initPos = initPos;
	_changeStateArea = changeStateArea;
}

Item::~Item(void)
{
}

bool		Item::mouseOnArea(t_pos mousePos)
{
	if ((mousePos.x >= _changeStateArea.TL.x && mousePos.x <= _changeStateArea.BR.x) &&
		(mousePos.y >= _changeStateArea.TL.y && mousePos.y <= _changeStateArea.BR.y))
		return true;
	return false;
}

void		Item::setName(std::string name)
{
	_name = name;
}

std::string	Item::getName()
{
	return _name;
}

void		Item::setInitPos(t_pos initPos)
{
	_initPos = initPos;
}

t_pos		Item::getInitPos()
{
	return _initPos;
}

void		Item::setChangeStateArea(t_area changeStateArea)
{
	_changeStateArea = changeStateArea;
}

t_area		Item::getChangeStateArea()
{
	return _changeStateArea;
}