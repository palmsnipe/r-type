#ifndef __MYTEXTURE_HH__
#define __MYTEXTURE_HH__

#include <SFML/Graphics/Texture.hpp>
#include <iostream>

class MyTexture :	public sf::Texture
{
public:
	MyTexture(void);
	~MyTexture(void);

	bool		create(int, int);
	bool		loadFromFile(std::string);
	void		operator=(sf::Texture);

private:
	int				_x;
	int				_y;
	std::string		_path;
};

#endif