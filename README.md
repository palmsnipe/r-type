Projet R-Type
=============

**R-Type** est un projet Epitech de 3e année, développé en C++ . Le but est de développer un jeu inspiré du jeu original R-Type.

## Liens utiles

  - Dossier partagé : [https://groups.live.com/rtype](https://groups.live.com/rtype/)

## Membres du projet
  - Cyril Morales
  - Yoni Goldberg
  - Guilhem Crespo
  - Romain Severe
  - Clément Compas